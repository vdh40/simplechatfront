import React, {Component} from 'react';
import {MessengerBox} from './Components/MessengerBox';
import {AuthBox} from './Components/AuthBox';
import {OnlineUsersListBox} from './Components/OnlineUsersListBox';
import {RootStore} from './Store';
import {inject, observer} from 'mobx-react';
import './App.css';
import PropTypes from 'prop-types';

@inject('store')
@observer
class App extends Component {
  get authStore() {
    return this.props.store.authStore;
  }

  get dialogsStore() {
    return this.props.store.dialogsStore;
  }

  render() {
    const {isUserLogged} = this.authStore;
    const {isCreatingDialog} = this.dialogsStore;

    // document.addEventListener('keydown', (e) => {
    //   if ((e.code === 'Enter') && (document.getElementById('SendBtnId'))) {
    //     document.getElementById('SendBtnId').click();
    //   } else if ((e.code === 'Enter') && (document.getElementById('EnterBtnId'))) {
    //     document.getElementById('EnterBtnId').click();
    //   } else if ((e.code === 'Escape') && (document.getElementById('BtnBackId'))) {
    //     document.getElementById('BtnBackId').click();
    //   }
    // });

    return (
      <div className="App">
        <div className="header">
          <div>VDисконнекте</div>
        </div>
        {isCreatingDialog ? <>
          <div className="mainBlockOpac">
            {isUserLogged ? <MessengerBox/> : <AuthBox/>}
          </div>
          <div className="onlineUsers">
            <OnlineUsersListBox/>
          </div>
        </> : <div className="mainBlock">
          {isUserLogged ? <MessengerBox/> : <AuthBox/>}
        </div>
        }
      </div>
    );
  }
}

App.propTypes = {
  store: PropTypes.objectOf(RootStore)
};

export default App;

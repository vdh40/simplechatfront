import React, {Component} from 'react';
import {OnlineUserItem} from './OnlineUserItem';
import {inject, observer} from "mobx-react";
import {RootStore} from "../../Store";
import './style.css'
import PropTypes from 'prop-types';

@inject('store')
@observer
class OnlineUsersListBox extends Component {
  get dialogsStore() {
    return this.props.store.dialogsStore;
  }

  render() {
    const {onlineUsers, clickOnUser} = this.dialogsStore;

    return (
      <div className="OnlineUsers">
        <span id="chUsr">Choose user:</span>
        {onlineUsers.map(({username}, i) =>
          <OnlineUserItem key={i} value={username} onClick={() => clickOnUser(username)}/>
        )}
      </div>
    )
  }
}

OnlineUsersListBox.propTypes = {
  store: PropTypes.objectOf(RootStore)
};

export {OnlineUsersListBox};
import React from 'react';
import './style.css'
import PropTypes from 'prop-types';

export const OnlineUserItem = ({value, onClick}) => (
  <div className="OnlineUserItem" onClick={onClick}>
    {value}
  </div>
);

OnlineUserItem.propTypes = {
  value: PropTypes.string,
  onClick: PropTypes.func
};
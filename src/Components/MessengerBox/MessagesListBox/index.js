import React, {Component} from 'react';
import {MessageItem} from './MessageItem';
import {inject, observer} from 'mobx-react';
import {RootStore} from "../../../Store";
import PropTypes from 'prop-types';

@inject('store')
@observer
class MessagesListBox extends Component {
  get dialogsStore() {
    return this.props.store.dialogsStore;
  }

  get authStore() {
    return this.props.store.authStore;
  }

  render() {
    const {currentDialog, clickOnBtnBack, nameOfInterlocutor} = this.dialogsStore;
    const {username} = this.authStore;

    return (
      <div className="MessagesBox">
        <div className="DialogInfoBar">
          <button className="BtnBack" id="BtnBackId" onClick={clickOnBtnBack}>Назад</button>
          <div className="InterlocutorName">{nameOfInterlocutor}</div>
          <div className="Avatar">
            <img className="SmallAvatarImg" src="ava.png" alt={nameOfInterlocutor}/>
          </div>
        </div>
        <div className="Messages" id="MessagesList">
          {
            currentDialog && currentDialog.map(({from: talker, msg: msgText, date}, i) =>
              <MessageItem key={i}
                           className={talker === username ? "RightSideMessageItem" : "LeftSideMessageItem"}
                           talker={talker}
                           message={msgText}
                           date={date.toString()}
              />
            )
          }
        </div>
      </div>
    )
  }
}

MessagesListBox.propTypes = {
  store: PropTypes.objectOf(RootStore)
};

export {MessagesListBox};
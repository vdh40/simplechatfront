import React from 'react';
import PropTypes from 'prop-types';

export const MessageItem = ({className, message}) => (
  <div className={className}>
      <span>{message}</span>
  </div>
);

MessageItem.propTypes = {
    className: PropTypes.string,
    talker: PropTypes.string,
    message: PropTypes.string,
    date: PropTypes.string
};
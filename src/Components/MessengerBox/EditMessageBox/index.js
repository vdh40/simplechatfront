import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {RootStore} from "../../../Store";
import PropTypes from 'prop-types';
import {observable} from "mobx";

@inject('store')
@observer
class EditMessageBox extends Component {
  @observable editValue = '';

  get dialogsStore() {
    return this.props.store.dialogsStore;
  }

  onEditInputChange = (e) => {
    this.editValue = e.target.value;
  };

  render() {
    const {activeDlgId, sendMessage} = this.dialogsStore;

    return (
      <div className="Edit">
        <input className="EditField" id="edit" value={this.editValue} onChange={this.onEditInputChange}
        onKeyPress={event => {
          if (event.key === "Enter") sendMessage(this.editValue, activeDlgId);
        }}/>
        <button className="SendButton" id="SendBtnId" onClick={() => sendMessage(this.editValue, activeDlgId)}>
          <img className="SendBtnIcon" src="send-btn-icon.png" alt="Отправить"/>
        </button>
      </div>
    )
  }
}

EditMessageBox.propTypes = {
  store: PropTypes.objectOf(RootStore)
};

export {EditMessageBox};
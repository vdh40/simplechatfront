import React, {Component} from 'react';
import {MessagesListBox} from './MessagesListBox';
import {DialogsListBox} from '../DialogsListBox';
import {EditMessageBox} from './EditMessageBox';
import {inject, observer} from "mobx-react";
import './style.css';
import {RootStore} from "../../Store";
import PropTypes from 'prop-types';

@inject('store')
@observer
class MessengerBox extends Component {
  get dialogsStore() {
    return this.props.store.dialogsStore;
  }

  render() {
    const {isDialogOpened} = this.dialogsStore;
    return (
      <div className="MessengerBox">
        {isDialogOpened ?
          <>
            <MessagesListBox/>
            <EditMessageBox/>
          </> : <DialogsListBox/>}
      </div>
    )
  }
}

MessengerBox.propTypes = {
  store: PropTypes.objectOf(RootStore)
};

export {MessengerBox};
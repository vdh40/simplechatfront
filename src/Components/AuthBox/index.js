import React, {Component} from 'react';
import {RootStore} from '../../Store';
import {inject, observer} from 'mobx-react';
import './style.css'
import {observable} from "mobx";
import PropTypes from 'prop-types';

@inject('store')
@observer
class AuthBox extends Component {
  @observable password = 'ca5106381';

  get authStore() {
    return this.props.store.authStore;
  }

  onUsernameInputChange = (e) => {
    this.authStore.username = e.target.value;
  };

  onPasswordInputChange = (e) => {
    this.password = e.target.value;
  };

  onButtonClick = (action) => {
    action(this.password);
    this.password = '';
  };

  render() {
    const {clickOnEnterBtn, clickOnRegisterBtn, username} = this.authStore;

    return (
      <div className="AuthBox">
        <input className="usernameEdit" value={username} onChange={this.onUsernameInputChange}/>
        <input className="passwordEdit" type="password" onChange={this.onPasswordInputChange} value={this.password}
               onKeyPress={(e) => {if (e.key === 'Enter') this.onButtonClick(clickOnEnterBtn)}}/>
        <div className="btns">
          <button className="enterBtn" id="EnterBtnId" onClick={() => this.onButtonClick(clickOnEnterBtn)}>Войти
          </button>
          <button className="regBtn" onClick={() => this.onButtonClick(clickOnRegisterBtn)}>Зарегистрироваться</button>
        </div>
      </div>
    )
  }
}

AuthBox.propTypes = {
  store: PropTypes.objectOf(RootStore)
};

export {AuthBox};
import React, {Component} from 'react';
import {DialogItem} from './DialogItem';
import {inject, observer} from 'mobx-react';
import {RootStore} from "../../Store";
import PropTypes from 'prop-types';

@inject('store')
@observer
class DialogsListBox extends Component {
  get dialogsStore() {
    return this.props.store.dialogsStore;
  }

  get authStore() {
    return this.props.store.authStore;
  }

  render() {
    const {dialogs, clickOnDialogItem} = this.dialogsStore;
    const {username} = this.authStore;

    return (
      <div className="Dialogs">
        <div className="TopDialogsDiv">
          <button className="CreateDlgBtn" onClick={this.dialogsStore.clickOnCreateDlgBtn}>+</button>
        </div>
        {
          dialogs.length !== 0 ? dialogs.map((dialog, i) => {
              const talker = (dialog.firstTalker === username) ? dialog.secondTalker : dialog.firstTalker;
              return (<DialogItem key={i} dialogId={dialog.id} talker={talker}
                                  onClick={() => clickOnDialogItem(dialog.id, talker)}/>);
            }) :
            <div className="EmptyDlgListMsg">
              Создайте диалог, чтобы начать общение!
            </div>
        }
      </div>
    )
  }
}

DialogsListBox.propTypes = {
  store: PropTypes.objectOf(RootStore)
};

export {DialogsListBox};
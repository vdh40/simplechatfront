import React from 'react';
import PropTypes from 'prop-types';

export const DialogItem = ({talker, onClick}) => (
  <div className="DialogItem" onClick={onClick}>
    <div className="Avatar">
      <img className="AvatarImg" src="ava.png" alt="Avatar"/>
    </div>
    <div className="DialogPreview">
      <div className="Name">{talker}</div>
      <div className="LastMsgPreview">msg</div>
    </div>
    <div className="LastMsgDate">00:01</div>
  </div>
);

DialogItem.propTypes = {
  talker: PropTypes.string,
  onClick: PropTypes.func
};
import {AuthStore} from "./AuthStore";
import {DialogsStore} from "./DialogsStore";

export class RootStore {
  constructor() {
    this.authStore = new AuthStore(this);
    this.dialogsStore = new DialogsStore(this);
  }
}
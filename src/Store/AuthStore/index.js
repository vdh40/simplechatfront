import {observable} from "mobx";
import sha256 from "crypto-js/sha256";

export class AuthStore {
  @observable username = 'vdh40';
  @observable isUserLogged = false;
  @observable ws;
  host = '192.168.1.64:8080';

  constructor(root) {
    this.root = root;
  }

  get dialogsStore() {
    return this.root.dialogsStore;
  }

  configureWebsocket = () => {
    this.ws = new WebSocket(`ws://${this.host}/?username=${this.username}`);

    this.ws.onmessage = (event) => {
      const data = JSON.parse(event.data);

      switch (data.msgType) {
        case 'dlgUpd':
          if (data.dialog.id === this.dialogsStore.activeDlgId) {
            this.dialogsStore.currentDialog.replace(data.dialog.messages);
          }
          break;

        case 'dlgListUpd':
          this.dialogsStore.dialogs.replace(data.dialogsList);
          break;

        case 'onlineUsersListUpd':
          this.dialogsStore.onlineUsers.replace(data.onlineUsersList);
          break;

        case 'chatCreated':
          this.dialogsStore.activeDlgId = data.dlgId;
          this.dialogsStore.updateCurDialog(data.dlgId);
          break;

        case 'error':
          alert(`Error received from server: ${data.errText}`);
          break;

        default:
          alert(`Unexpected message get from server: ${event.data}`);
      }
    }
  };

  clickOnEnterBtn = (password) => {
    fetch(`http://${this.host}/auth`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        username: this.username,
        hash: String(sha256(this.username + password))
      })
    }).then(response => {
      if (response.status === 200) {
        this.isUserLogged = true;
        this.configureWebsocket();
      } else {
        alert('Authorization failed!');
      }
    }).catch(err => alert(err));
  };

  clickOnRegisterBtn = (password) => {
    fetch(`http://${this.host}/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        username: this.username,
        hash: String(sha256(this.username + password))
      })
    }).then(response => {
      if (response.status === 200) {
        alert('Registered successfully!');
      } else {
        alert('Registration failed!');
      }
    }).catch(err => alert(err));
  };
}
import {action, observable} from "mobx";

export class DialogsStore {
  @observable dialogs = [];
  @observable currentDialog = [];
  @observable activeDlgId = 0;
  @observable nameOfInterlocutor = '';
  @observable onlineUsers = [];
  @observable isCreatingDialog = false;
  @observable isDialogOpened = false;

  constructor(root) {
    this.root = root;
  }

  get authStore() {
    return this.root.authStore;
  }

  updateCurDialog = (dlgId) => {
    this.authStore.ws.send(JSON.stringify({
      msgType: 'getDialogById',
      dialogId: dlgId
    }));
  };

  getChatsOfUser = (username) => {
    this.authStore.ws.send(JSON.stringify({
      msgType: 'getDialogsList',
      username: username
    }));
  };

  getOnlineUsersList = () => {
    this.authStore.ws.send(JSON.stringify({
      msgType: 'getOnlineUsersList'
    }));
  };

  createDialog = (firstTalker, secondTalker) => {
    this.authStore.ws.send(JSON.stringify({
      msgType: 'createChat',
      firstTalker: firstTalker,
      secondTalker: secondTalker
    }))
  };

  sendMessage = (message, dlgId) => {
    if (/^\s*$/.test(document.getElementById('edit').value)) {
      return;
    }

    const {username} = this.authStore;

    new Promise((resolve, reject) => {
      this.authStore.ws.send(JSON.stringify({
        msgType: 'postMessage',
        dialogId: dlgId,
        username: username,
        msg: message
      }));

      setTimeout(() => resolve('Ok'), 10);
    }).then(res => {
      const messagesDiv = document.getElementById('MessagesList');
      messagesDiv.scrollTop = messagesDiv.scrollHeight;
    });

    document.getElementById('edit').value = '';
  };

  @action
  clickOnCreateDlgBtn = () => {
    this.getOnlineUsersList();
    this.isCreatingDialog = true;
  };

  @action
  clickOnDialogItem = (dlgId, talker) => {
    this.isDialogOpened = true;
    this.activeDlgId = dlgId;
    this.updateCurDialog(dlgId);
    this.nameOfInterlocutor = talker;
    setTimeout(() => {
      const messagesDiv = document.getElementById('MessagesList');
      messagesDiv.scrollTop = messagesDiv.scrollHeight;
    }, 10);
  };

  @action
  clickOnUser = (companion) => {
    const {username} = this.authStore;
    this.createDialog(username, companion);
    this.isCreatingDialog = false;
    this.nameOfInterlocutor = companion;
  };

  @action
  clickOnBtnBack = () => {
    this.isDialogOpened = false;
  }
}